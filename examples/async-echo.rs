use foxtrot::Async;
use join_all::join_all;
use std::net::{SocketAddr, SocketAddrV4, TcpStream};

async fn echo_to(port: u16) -> Result<(), Box<dyn std::error::Error>> {
    let sockaddr = SocketAddr::V4(SocketAddrV4::new([127, 0, 0, 1].into(), port));

    let stream = TcpStream::connect(sockaddr)?;
    stream.set_nonblocking(true)?;
    let mut stream = Async::new(stream)?;

    println!("Connected");

    loop {
        let mut buf = [0; 1024];
        if let Err(e) = stream.read(&mut buf).await {
            if e.kind() == std::io::ErrorKind::BrokenPipe {
                break;
            }

            return Err(e.into());
        }
        if let Err(e) = stream.write_all(&buf).await {
            if e.kind() == std::io::ErrorKind::BrokenPipe {
                break;
            }

            return Err(e.into());
        }
    }

    println!("Connection closed");

    Ok(())
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    foxtrot::block_on(async move {
        for res in join_all(vec![Box::pin(echo_to(4444)), Box::pin(echo_to(4445))]).await {
            res?;
        }

        Ok(())
    })?
}
