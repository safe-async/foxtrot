use foxtrot::Error;
use join_all::join_all;
use std::{future::Future, pin::Pin, time::Duration};

fn main() -> Result<(), Error> {
    foxtrot::block_on(async move {
        let f1 = Box::pin(async move {
            foxtrot::time::sleep(Duration::from_secs(4)).await;
            println!("waited 4");
        }) as Pin<Box<dyn Future<Output = ()>>>;

        let f2 = Box::pin(async move {
            foxtrot::time::sleep(Duration::from_secs(2)).await;
            println!("waited 2");
        });

        let f3 = Box::pin(async move {
            let mut interval = foxtrot::time::interval(Duration::from_secs(1));

            for count in 0..5 {
                interval.tick().await;
                println!("count {}", count);
            }
        });

        join_all(vec![f1, f2, f3]).await;
    })
}
