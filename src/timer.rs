use std::{
    future::Future,
    pin::Pin,
    task::{Context, Poll},
    time::{Duration, Instant},
};

use crate::{reactor::RemoveFlag, ReactorRef};

pub struct Timer {
    created: Instant,
    duration: Duration,
    remove_flag: Option<RemoveFlag>,
}

impl Timer {
    pub fn new(duration: Duration) -> Self {
        Timer {
            duration,
            created: Instant::now(),
            remove_flag: None,
        }
    }
}

impl Future for Timer {
    type Output = ();

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let now = Instant::now();

        let diff = now.checked_duration_since(self.created);

        let duration = if let Some(diff) = diff {
            if self.duration <= diff {
                return Poll::Ready(());
            }

            self.duration.checked_sub(diff)
        } else {
            None
        };
        let duration = duration.unwrap_or(self.duration);

        self.remove_flag = Some(
            ReactorRef::with(|mut reactor| reactor.add_timer(duration, cx.waker().clone()))
                .unwrap(),
        );

        Poll::Pending
    }
}
