use std::time::{Duration, Instant};

pub use crate::timer::Timer;

pub fn sleep(duration: Duration) -> Timer {
    Timer::new(duration)
}

pub fn interval(duration: Duration) -> Interval {
    Interval {
        next: Instant::now(),
        duration,
    }
}

pub fn interval_at(instant: Instant, duration: Duration) -> Interval {
    Interval {
        next: instant,
        duration,
    }
}

pub fn timeout<F: std::future::Future + Unpin>(duration: Duration, future: F) -> Timeout<F> {
    Timeout {
        timer: Timer::new(duration),
        future,
    }
}

pub struct Timeout<F> {
    timer: Timer,
    future: F,
}

pub struct Interval {
    next: Instant,
    duration: Duration,
}

impl Interval {
    pub fn tick(&mut self) -> Timer {
        let now = Instant::now();

        while self.next + self.duration <= now {
            self.next += self.duration;
        }

        let duration = if let Some(duration) = now.checked_duration_since(self.next) {
            duration
        } else if let Some(duration) = self.next.checked_duration_since(now) {
            duration
        } else {
            Duration::from_secs(0)
        };

        self.next += self.duration;

        Timer::new(duration)
    }
}

impl<F> std::future::Future for Timeout<F>
where
    F: std::future::Future + Unpin,
{
    type Output = std::io::Result<F::Output>;

    fn poll(
        self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Self::Output> {
        let this = self.get_mut();

        if std::pin::Pin::new(&mut this.timer).poll(cx).is_ready() {
            return std::task::Poll::Ready(Err(std::io::ErrorKind::TimedOut.into()));
        }

        std::pin::Pin::new(&mut this.future).poll(cx).map(Ok)
    }
}
