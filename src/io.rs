use crate::reactor::ReactorRef;
use polldance::net::{
    TcpListener, TcpListenerBuilder, TcpStream, TcpStreamBuilder, UdpSocket, UdpSocketBuilder,
};
use std::{
    future::{poll_fn, Future},
    net::SocketAddr,
    pin::Pin,
    sync::Arc,
    task::{Context, Poll},
};

pub use polldance::{
    fd::{AsFd, OwnedFd},
    io::{Error, Nonblocking, ReadBytes, Result},
    Readiness,
};

macro_rules! poll_nonblocking {
    ($expr:expr) => {{
        match $expr {
            Ok(Nonblocking::Ready(val)) => return Poll::Ready(Ok(val)),
            Err(e) => return Poll::Ready(Err(e)),
            Ok(Nonblocking::WouldBlock) => {}
        }
    }};
}

fn nonblocking<T>(res: Result<T>) -> Result<Nonblocking<T>> {
    match res {
        Ok(t) => Ok(Nonblocking::Ready(t)),
        Err(e) if e.kind() == std::io::ErrorKind::WouldBlock => Ok(Nonblocking::WouldBlock),
        Err(e) => Err(e),
    }
}

pub struct Async<T: AsFd + 'static> {
    source: Arc<OwnedFd>,
    io: T,
}

struct BindTcpListener {
    io: Option<Result<Arc<TcpListenerBuilder>>>,
}

struct BindUdpSocket {
    io: Option<Result<Arc<UdpSocketBuilder>>>,
}

struct Connect {
    io: Option<Result<Arc<TcpStreamBuilder>>>,
}

impl<T: AsFd + 'static> Drop for Async<T> {
    fn drop(&mut self) {
        let _ = ReactorRef::with(|mut reactor| reactor.deregister(&self.source));
    }
}

impl Async<UdpSocket> {
    pub async fn bind<A: Into<SocketAddr>>(socket_address: A) -> Result<Async<UdpSocket>> {
        BindUdpSocket {
            io: Some(UdpSocket::bind(socket_address).map(Arc::new)),
        }
        .await
    }

    pub async fn recv_from(&mut self, buf: &mut [u8]) -> Result<(usize, SocketAddr)> {
        poll_fn(|cx| self.poll_recv_from(cx, buf)).await
    }

    pub fn poll_recv_from(
        &self,
        cx: &mut Context<'_>,
        buf: &mut [u8],
    ) -> Poll<Result<(usize, SocketAddr)>> {
        match self.recv_from_nonblocking(buf) {
            Ok(Nonblocking::Ready((size, Some(addr)))) => return Poll::Ready(Ok((size, addr))),
            Ok(Nonblocking::Ready(_)) => return Poll::Ready(Err(std::io::ErrorKind::Other.into())),
            Ok(Nonblocking::WouldBlock) => {}
            Err(e) => return Poll::Ready(Err(e)),
        };

        self.register_readable(cx);

        Poll::Pending
    }

    pub fn recv_from_nonblocking(
        &self,
        buf: &mut [u8],
    ) -> Result<Nonblocking<(usize, Option<SocketAddr>)>> {
        self.io.try_recv_from(buf)
    }

    pub async fn send_to(&mut self, buf: &[u8], target: SocketAddr) -> Result<usize> {
        poll_fn(|cx| self.poll_send_to(cx, buf, target)).await
    }

    pub fn poll_send_to(
        &self,
        cx: &mut Context<'_>,
        buf: &[u8],
        target: SocketAddr,
    ) -> Poll<Result<usize>> {
        poll_nonblocking!(self.send_to_nonblocking(buf, target));

        self.register_writable(cx);

        Poll::Pending
    }

    pub fn send_to_nonblocking(
        &self,
        buf: &[u8],
        target: SocketAddr,
    ) -> Result<Nonblocking<usize>> {
        self.io.try_send_to(buf, target)
    }
}

impl Async<TcpListener> {
    pub async fn bind<A: Into<SocketAddr>>(socket_address: A) -> Result<Async<TcpListener>> {
        BindTcpListener {
            io: Some(TcpListener::bind(socket_address).map(Arc::new)),
        }
        .await
    }

    pub fn poll_accept(
        &mut self,
        cx: &mut Context<'_>,
    ) -> Poll<Result<(Async<TcpStream>, Option<SocketAddr>)>> {
        match self.io.try_accept() {
            Ok(Nonblocking::Ready((stream, addr))) => {
                return Poll::Ready(Async::new(stream).map(|stream| (stream, addr)))
            }
            Ok(Nonblocking::WouldBlock) => {}
            Err(e) => return Poll::Ready(Err(e)),
        };

        self.register_readable(cx);

        Poll::Pending
    }

    pub async fn accept(&mut self) -> Result<(Async<TcpStream>, Option<SocketAddr>)> {
        poll_fn(|cx| self.poll_accept(cx)).await
    }
}

impl Async<TcpStream> {
    pub async fn connect<A: Into<SocketAddr>>(socket_address: A) -> Result<Async<TcpStream>> {
        Connect {
            io: Some(polldance::net::TcpStream::connect(socket_address).map(Arc::new)),
        }
        .await
    }

    pub async fn connect_with_bind<A: Into<SocketAddr>, B: Into<SocketAddr>>(
        socket_address: A,
        bind_address: Option<B>,
    ) -> Result<Async<TcpStream>> {
        Connect {
            io: Some(
                polldance::net::TcpStream::connect_with_bind(socket_address, bind_address)
                    .map(Arc::new),
            ),
        }
        .await
    }
}

impl<T: AsFd + 'static> Async<T> {
    pub fn new(io: T) -> Result<Self> {
        let source = Arc::new(polldance::fd::try_clone(&io)?);
        Ok(Async { source, io })
    }

    pub async fn ready(&mut self, interests: Readiness) -> Result<Readiness>
    where
        T: Unpin,
    {
        poll_fn(|cx| self.poll_ready(cx, interests)).await
    }

    pub fn poll_ready(
        &mut self,
        cx: &mut Context<'_>,
        interests: Readiness,
    ) -> Poll<Result<Readiness>> {
        let interests = interests | Readiness::hangup();

        poll_nonblocking!(polldance::io::try_ready(&self.io, interests));

        self.register_interest(cx, interests);

        Poll::Pending
    }

    fn register_interest(&self, cx: &mut Context<'_>, interests: Readiness) {
        ReactorRef::with(|mut reactor| {
            reactor.register(
                Arc::clone(&self.source),
                cx.waker().clone(),
                interests | Readiness::hangup(),
            );
        })
        .unwrap();
    }

    pub async fn read_with<R>(&self, op: impl Fn(&T) -> Result<R>) -> Result<R> {
        poll_fn(|cx| {
            poll_nonblocking!(nonblocking((op)(&self.io)));

            self.register_readable(cx);

            Poll::Pending
        })
        .await
    }

    pub async fn read_with_mut<R>(&mut self, op: impl Fn(&mut T) -> Result<R>) -> Result<R> {
        poll_fn(|cx| {
            poll_nonblocking!(nonblocking((op)(&mut self.io)));

            self.register_readable(cx);

            Poll::Pending
        })
        .await
    }

    pub async fn read(&mut self, buf: &mut [u8]) -> Result<ReadBytes>
    where
        T: std::io::Read + Unpin,
    {
        poll_fn(|cx| self.poll_read(cx, buf)).await.map(From::from)
    }

    fn register_readable(&self, cx: &mut Context<'_>) {
        self.register_interest(cx, Readiness::read());
    }

    pub fn poll_read(&mut self, cx: &mut Context<'_>, buf: &mut [u8]) -> Poll<Result<usize>>
    where
        T: std::io::Read,
    {
        poll_nonblocking!(self.read_nonblocking(buf));

        self.register_readable(cx);

        Poll::Pending
    }

    pub fn read_nonblocking(&mut self, buf: &mut [u8]) -> Result<Nonblocking<usize>>
    where
        T: std::io::Read,
    {
        nonblocking(std::io::Read::read(&mut self.io, buf))
    }

    pub async fn read_exact(&mut self, bytes: &mut [u8]) -> Result<usize>
    where
        T: std::io::Read + Unpin,
    {
        let mut start = 0;

        while start < bytes.len() {
            match self.read(&mut bytes[start..]).await? {
                ReadBytes::EOF => break,
                ReadBytes::Read(n) => start += usize::from(n),
            }
        }

        Ok(start)
    }

    pub async fn write_with<R>(&self, op: impl Fn(&T) -> Result<R>) -> Result<R> {
        poll_fn(|cx| {
            poll_nonblocking!(nonblocking((op)(&self.io)));

            self.register_writable(cx);

            Poll::Pending
        })
        .await
    }

    pub async fn write_with_mut<R>(&mut self, op: impl Fn(&mut T) -> Result<R>) -> Result<R> {
        poll_fn(|cx| {
            poll_nonblocking!(nonblocking((op)(&mut self.io)));

            self.register_writable(cx);

            Poll::Pending
        })
        .await
    }

    pub async fn write(&mut self, buf: &[u8]) -> Result<usize>
    where
        T: std::io::Write + Unpin,
    {
        poll_fn(|cx| self.poll_write(cx, buf)).await
    }

    fn register_writable(&self, cx: &mut Context<'_>) {
        self.register_interest(cx, Readiness::write());
    }

    pub fn poll_write(&mut self, cx: &mut Context<'_>, buf: &[u8]) -> Poll<Result<usize>>
    where
        T: std::io::Write,
    {
        poll_nonblocking!(self.write_nonblocking(buf));

        self.register_writable(cx);

        Poll::Pending
    }

    pub fn write_nonblocking(&mut self, buf: &[u8]) -> Result<Nonblocking<usize>>
    where
        T: std::io::Write,
    {
        nonblocking(std::io::Write::write(&mut self.io, buf))
    }

    pub async fn write_all(&mut self, bytes: &[u8]) -> Result<()>
    where
        T: std::io::Write + Unpin,
    {
        let mut start = 0;

        while start < bytes.len() {
            start += self.write(&bytes[start..]).await?;
        }

        Ok(())
    }

    pub async fn flush(&mut self) -> Result<()>
    where
        T: std::io::Write + Unpin,
    {
        poll_fn(|cx| self.poll_flush(cx)).await
    }

    pub fn poll_flush(&mut self, cx: &mut Context<'_>) -> Poll<Result<()>>
    where
        T: std::io::Write,
    {
        poll_nonblocking!(self.flush_nonblocking());

        self.register_writable(cx);

        Poll::Pending
    }

    pub fn flush_nonblocking(&mut self) -> Result<Nonblocking<()>>
    where
        T: std::io::Write,
    {
        match std::io::Write::flush(&mut self.io) {
            Ok(()) => Ok(Nonblocking::Ready(())),
            Err(e) if e.kind() == std::io::ErrorKind::WouldBlock => Ok(Nonblocking::WouldBlock),
            Err(e) => Err(e),
        }
    }
}

#[cfg(feature = "tokio-compat")]
impl<T> tokio::io::AsyncRead for Async<T>
where
    T: std::io::Read + AsFd + Unpin,
{
    fn poll_read(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &mut tokio::io::ReadBuf,
    ) -> Poll<Result<()>> {
        let this = self.get_mut();

        match this.read_nonblocking(buf.initialize_unfilled()) {
            Ok(Nonblocking::Ready(n)) => {
                buf.advance(n);
                return Poll::Ready(Ok(()));
            }
            Ok(Nonblocking::WouldBlock) => {}
            Err(e) => return Poll::Ready(Err(e)),
        };

        this.register_readable(cx);

        Poll::Pending
    }
}

#[cfg(feature = "futures-compat")]
impl<T> futures_io::AsyncRead for Async<T>
where
    T: std::io::Read + AsFd + Unpin,
{
    fn poll_read(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &mut [u8],
    ) -> Poll<Result<usize>> {
        self.get_mut().poll_read(cx, buf)
    }
}

#[cfg(feature = "tokio-compat")]
impl<'a, T> tokio::io::AsyncWrite for Async<T>
where
    T: std::io::Write + AsFd + Unpin,
{
    fn poll_write(self: Pin<&mut Self>, cx: &mut Context<'_>, buf: &[u8]) -> Poll<Result<usize>> {
        self.get_mut().poll_write(cx, buf)
    }

    fn poll_flush(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<()>> {
        self.get_mut().poll_flush(cx)
    }

    fn poll_shutdown(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<()>> {
        self.get_mut().poll_flush(cx)
    }
}

#[cfg(feature = "futures-compat")]
impl<'a, T> futures_io::AsyncWrite for Async<T>
where
    T: std::io::Write + AsFd + Unpin,
{
    fn poll_write(self: Pin<&mut Self>, cx: &mut Context<'_>, buf: &[u8]) -> Poll<Result<usize>> {
        self.get_mut().poll_write(cx, buf)
    }

    fn poll_flush(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<()>> {
        self.get_mut().poll_flush(cx)
    }

    fn poll_close(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<()>> {
        self.get_mut().poll_flush(cx)
    }
}

impl Future for BindTcpListener {
    type Output = Result<Async<TcpListener>>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = self.get_mut();

        match this.io.take().unwrap() {
            Ok(builder) => {
                let builder = match Arc::try_unwrap(builder) {
                    Ok(builder) => builder,
                    Err(arc) => {
                        this.io = Some(Ok(arc));
                        return Poll::Pending;
                    }
                };

                match builder.try_finish() {
                    Ok(Ok(listener)) => Poll::Ready(Async::new(listener)),
                    Ok(Err(builder)) => {
                        let builder = Arc::new(builder);

                        ReactorRef::with(|mut reactor| {
                            reactor.register(
                                Arc::clone(&builder),
                                cx.waker().clone(),
                                Readiness::read(),
                            );
                        })
                        .unwrap();

                        Poll::Pending
                    }
                    Err(e) => Poll::Ready(Err(e)),
                }
            }
            Err(e) => Poll::Ready(Err(e)),
        }
    }
}

impl Future for BindUdpSocket {
    type Output = Result<Async<UdpSocket>>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = self.get_mut();

        match this.io.take().unwrap() {
            Ok(builder) => {
                let builder = match Arc::try_unwrap(builder) {
                    Ok(builder) => builder,
                    Err(arc) => {
                        this.io = Some(Ok(arc));
                        return Poll::Pending;
                    }
                };

                match builder.try_finish() {
                    Ok(Ok(socket)) => Poll::Ready(Async::new(socket)),
                    Ok(Err(builder)) => {
                        let builder = Arc::new(builder);

                        ReactorRef::with(|mut reactor| {
                            reactor.register(
                                Arc::clone(&builder),
                                cx.waker().clone(),
                                Readiness::read(),
                            );
                        })
                        .unwrap();

                        Poll::Pending
                    }
                    Err(e) => Poll::Ready(Err(e)),
                }
            }
            Err(e) => Poll::Ready(Err(e)),
        }
    }
}

impl Future for Connect {
    type Output = Result<Async<TcpStream>>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = self.get_mut();

        match this.io.take().unwrap() {
            Ok(builder) => {
                let builder = match Arc::try_unwrap(builder) {
                    Ok(builder) => builder,
                    Err(arc) => {
                        this.io = Some(Ok(arc));
                        return Poll::Pending;
                    }
                };

                match builder.try_finish() {
                    Ok(Ok(stream)) => Poll::Ready(Async::new(stream)),
                    Ok(Err(builder)) => {
                        let builder = Arc::new(builder);

                        ReactorRef::with(|mut reactor| {
                            reactor.register(
                                Arc::clone(&builder),
                                cx.waker().clone(),
                                Readiness::write(),
                            );
                        })
                        .unwrap();

                        this.io = Some(Ok(builder));
                        Poll::Pending
                    }
                    Err(e) => Poll::Ready(Err(e)),
                }
            }
            Err(e) => Poll::Ready(Err(e)),
        }
    }
}
