use polldance::{fd::AsFd, Key, NotifyToken, PollManager, Readiness};
use std::{
    cell::RefCell,
    collections::{BTreeMap, HashMap},
    future::Future,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc, Mutex, MutexGuard,
    },
    task::{Context, Poll, Wake, Waker},
    time::{Duration, Instant},
};

thread_local! {
    static REACTOR: RefCell<Option<Arc<Mutex<ReactorState>>>> = RefCell::new(None);
}

#[derive(Debug)]
pub enum Error {
    IO(polldance::io::Error),
    AlreadyInitialized,
    Uninitialized,
}

pub struct Reactor {
    state: Arc<Mutex<ReactorState>>,
}

pub struct ReactorRef<'a> {
    state: MutexGuard<'a, ReactorState>,
}

pub struct RemoveFlag {
    inner: Arc<AtomicBool>,
}

struct ReactorState {
    now: Instant,
    poller: PollManager,
    io: HashMap<Key, Vec<(Readiness, Waker)>>,
    timers: BTreeMap<Instant, Vec<(Arc<AtomicBool>, Waker)>>,
}

struct IoWaker {
    notify: NotifyToken,
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::IO(ref e) => std::fmt::Display::fmt(e, f),
            Self::AlreadyInitialized => {
                write!(f, "A Reactor has already been initialized on this thread")
            }
            Self::Uninitialized => write!(f, "No reactor has been initialized on this thread"),
        }
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        if let Self::IO(ref e) = self {
            return Some(e);
        }

        None
    }
}

impl ReactorRef<'_> {
    pub fn with<F, T>(callback: F) -> Result<T, Error>
    where
        F: for<'a> FnOnce(ReactorRef<'a>) -> T,
        T: 'static,
    {
        REACTOR.with(|reactor| {
            let guard = reactor.borrow_mut();

            if let Some(state) = guard.as_ref() {
                let res = callback(ReactorRef {
                    state: state.lock().unwrap(),
                });

                return Ok(res);
            }

            Err(Error::Uninitialized)
        })
    }

    pub fn register<A: AsFd + 'static>(&mut self, io: Arc<A>, waker: Waker, interests: Readiness) {
        self.state.register(io, waker, interests)
    }

    pub fn add_timer(&mut self, duration: Duration, waker: Waker) -> RemoveFlag {
        self.state.add_timer(duration, waker)
    }

    pub fn deregister<A: AsFd + 'static>(&mut self, io: &Arc<A>) {
        self.state.deregister(io)
    }

    pub fn update_interests<A: AsFd + 'static>(
        &mut self,
        io: &Arc<A>,
        callback: impl Fn(Readiness) -> Readiness,
    ) {
        self.state.update_interests(io, callback);
    }
}

impl From<polldance::io::Error> for Error {
    fn from(e: polldance::io::Error) -> Self {
        Error::IO(e)
    }
}

impl Drop for Reactor {
    fn drop(&mut self) {
        REACTOR.with(|reactor| {
            reactor.borrow_mut().take();
        });
    }
}

impl Wake for IoWaker {
    fn wake(self: Arc<Self>) {
        self.wake_by_ref()
    }

    fn wake_by_ref(self: &Arc<Self>) {
        let _ = self.notify.clone().notify();
    }
}

impl Reactor {
    pub fn new() -> Result<Self, Error> {
        let state = ReactorState::init()?;

        Ok(Self { state })
    }

    pub fn block_on<F: Future>(&self, future: F) -> Result<F::Output, Error> {
        let mut pinned = Box::pin(future);

        let notifier = self.state.lock().unwrap().poller.notifier();

        let io_waker = Arc::new(IoWaker { notify: notifier });
        let waker = Arc::clone(&io_waker).into();
        let mut context = Context::from_waker(&waker);

        loop {
            if let Poll::Ready(output) = pinned.as_mut().poll(&mut context) {
                return Ok(output);
            }

            self.state.lock().unwrap().poll()?;
        }
    }
}

impl Drop for RemoveFlag {
    fn drop(&mut self) {
        self.inner.store(true, Ordering::Release);
    }
}

impl ReactorState {
    fn init() -> Result<Arc<Mutex<Self>>, Error> {
        REACTOR.with(|reactor| {
            let mut guard = reactor.borrow_mut();

            if guard.is_some() {
                Err(Error::AlreadyInitialized)
            } else {
                let this = Arc::new(Mutex::new(Self {
                    now: Instant::now(),
                    poller: PollManager::new()?,
                    io: HashMap::new(),
                    timers: BTreeMap::new(),
                }));

                *guard = Some(Arc::clone(&this));

                Ok(this)
            }
        })
    }

    fn register<A: AsFd + 'static>(&mut self, io: Arc<A>, waker: Waker, interests: Readiness) {
        let key = self.poller.register(io, interests);
        let entry = self.io.entry(key).or_insert_with(Vec::new);

        for (ntrst, wkr) in entry.iter_mut() {
            if waker.will_wake(wkr) {
                *ntrst = *ntrst | interests;
                return;
            }
        }

        entry.push((interests, waker));
    }

    fn deregister<A: AsFd + 'static>(&mut self, io: &Arc<A>) {
        self.poller.deregister(io);
    }

    fn update_interests<A: AsFd + 'static>(
        &mut self,
        io: &Arc<A>,
        callback: impl Fn(Readiness) -> Readiness,
    ) {
        self.poller.update_interests(io, callback);
    }

    fn poll(&mut self) -> Result<(), Error> {
        let now = self.now();

        let first_wakeup = self.timers.keys().copied().next();

        let timeout = first_wakeup.map(|instant| {
            instant
                .checked_duration_since(now)
                .map(|duration| duration.as_millis().try_into().unwrap_or(i32::MAX))
                .unwrap_or(0)
        });

        for (key, readiness) in self.poller.poll(timeout)? {
            if let Some((key, vec)) = self.io.remove_entry(&key) {
                let new_vec = vec
                    .into_iter()
                    .filter_map(|(interests, waker)| {
                        if interests.is_intersect(readiness) {
                            waker.wake();
                            return None;
                        }

                        Some((interests, waker))
                    })
                    .collect::<Vec<_>>();

                let readiness = new_vec
                    .iter()
                    .fold(Readiness::empty(), |readiness, (current, _)| {
                        readiness | *current
                    });

                if new_vec.is_empty() || readiness.is_empty() {
                    self.poller.deregister_by_key(key);
                } else {
                    self.poller.update_interests_by_key(&key, |_| readiness);
                    self.io.insert(key, new_vec);
                }
            }
        }

        self.check_timers();

        Ok(())
    }

    fn now(&mut self) -> Instant {
        let now = Instant::now();

        if now.duration_since(self.now) >= Duration::from_millis(1) {
            self.now = now;
        }

        self.now
    }

    fn add_timer(&mut self, duration: Duration, waker: Waker) -> RemoveFlag {
        let now = self.now();

        let expires = now + duration;

        let flag = Arc::new(AtomicBool::new(false));

        self.timers
            .entry(expires)
            .or_insert_with(Vec::new)
            .push((Arc::clone(&flag), waker));

        RemoveFlag { inner: flag }
    }

    fn check_timers(&mut self) {
        let now = self.now();

        let expired = self
            .timers
            .keys()
            .copied()
            .take_while(|instant| *instant <= now)
            .collect::<Vec<_>>();

        for timer in expired {
            if let Some(wakers) = self.timers.remove(&timer) {
                for (flag, waker) in wakers {
                    if !flag.load(Ordering::Acquire) {
                        waker.wake();
                    }
                }
            }
        }
    }
}
