pub mod io;
mod reactor;
pub mod time;
mod timer;

use std::future::Future;

pub use io::Async;
pub use reactor::{Error, Reactor, ReactorRef};

pub fn block_on<F: Future>(future: F) -> Result<F::Output, Error> {
    Reactor::new()?.block_on(future)
}

pub mod net {
    pub use polldance::net::{TcpListener, TcpStream, UdpSocket};
}
